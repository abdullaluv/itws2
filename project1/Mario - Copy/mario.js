var lastKeyPressed_left_or_right = "right";
var max_frame = 0;
var frame = 0;
var game = {}
var coordinates = {x:0, y:0}
var showStartUpScreen = true
var showLives = 0
var mouse_pressed = false
var collidedWithEnemy = false
var level = 1

//Sounds
var jump = new Audio()
var gameover_sound = new Audio()
jump.src = "Sounds/jump.mp3"
gameover_sound.src = "Sounds/gameover.mp3"
var main_music = new Audio()
main_music.src = "Sounds/main.mp3"
var coin_taken = []
for(i=0;i<10;i++) {
	coin_taken[i] = new Audio()
	coin_taken[i].src = "Sounds/coin.mp3"
}

//Images that i usied in game.
var background_canvas = new Image()
var coin = new Image()
var forward_mario = new Image()
var mario_rev = new Image()
var brick = new Image()
var brick2 = new Image()
var brick3 = new Image()
var enemy = new Image()

brick3.src = "Images/brick3.jpg"
background_canvas.src = "Images/fundo.png"
enemy.src = "Images/enemy.png"
coin.src = "Images/coin.gif"
brick2.src = "Images/brick2.png";
brick.src = "Images/brick.png";
forward_mario.src = "Images/mario.png";
mario_rev.src = "Images/mario_rev.gif";

//Getting context.
var canvas = document.getElementById("mario_game")
var context = canvas.getContext("2d")

//KeyBoard keys currently pressed.

game.keyboard = []
for(i=0;i<256;i++) {
	game.keyboard.push(0);
}

window.onkeyup = function(event) {
	game.keyboard[getKey(event)] = 0;
}

window.onmousedown = function() {
	mouse_pressed = true;
}

window.onmouseup = function() {
	mouse_pressed = false;
}

window.onkeydown = function(event) {
	if(event.keyCode == 37) lastKeyPressed_left_or_right = "left";
	if(event.keyCode == 39) lastKeyPressed_left_or_right = "right";
	game.keyboard[getKey(event)] = 1;
}


//Properties of canvas.
game.canvas = {}
game.canvas.width = 600
game.canvas.height = 500
canvas.setAttribute("width", game.canvas.width+"px")
canvas.setAttribute("height", game.canvas.height+"px")

// Properties of player.
game.player = {}
game.player.collided = false
game.player.collided_direction = ""
game.player.x = 130
game.player.y = 130
game.player.width = 30
game.player.height = 40
game.player.gravity = 0.06
game.player.velocity_x = 0
game.player.velocity_y = 0
game.player.prevPosition = game.player
game.player.lives = 3
game.player.coins_taken = 0

game.player.draw_player = function() {

	if(lastKeyPressed_left_or_right == "left") mario = mario_rev;
	else mario = forward_mario;
	
	context.drawImage(mario, game.player.x - frame, game.player.y, game.player.width, game.player.height);

}

game.player.changeXY = function() {
		if(game.player.collided == true && game.keyboard[38] && game.player.collided_direction=="down") {
			jump.play()
			game.player.velocity_y = -3
		}
		if(game.keyboard[37] && game.player.x > max_frame) { game.player.x -=2;  }
		if(game.keyboard[39]  && game.player.collided_direction != "right") {
			if(game.keyboard[16]) game.player.x +=1;
			game.player.x +=2;			
		}
		if(game.player.collided == true && game.player.velocity_y > 0) return;
		game.player.velocity_y += game.player.gravity
		game.player.y += game.player.velocity_y
}

game.detectCollision = function(box1, box2) {
	return (box1.x <= box2.x + box2.width && box2.x <= box1.x + box1.width && box1.y <= box2.y + box2.height && box2.y <= box1.y + box1.height)
}

game.detectCollisions = function() {
	for(i=0;i<game.platforms.length;i++) {
		if(game.detectCollision(game.player, game.platforms[i])) {
		
			game.player.collided = true;
			
			if(game.player.y + game.player.height - game.platforms[i].y < 5) {
				game.player.collided_direction = "down";
				game.player.y = game.platforms[i].y - game.player.height;
				return;
			}
			
			else if(Math.abs(game.player.y - game.platforms[i].height - game.platforms[i].y) < 5) {
				game.player.collided_direction = "up";
				game.player.y = game.platforms[i].y + game.platforms[i].height + 1
				game.player.velocity_y = 0
				game.player.collided = false;
				return;
			}
			
			
			
			else if(game.keyboard[39]) {
				game.player.collided_direction = "right";
				game.player.x = game.platforms[i].x - game.player.width - 1;
				game.player.velocity_y = 0;
				return;
			}
			else if(game.keyboard[37]) {
				game.player.collided = false;
				game.player.x = game.platforms[i].x + game.platforms[i].width + 1
				game.keyboard.collided_direction = "left";
				game.player.velocity_y = 0;
				return;
			}
			
			else if(game.keyboard[38]) game.player.velocity_y = 0;
		
			
			return;
		}	
		
		else {
			game.player.collided = false;
			game.player.collided_direction = "none";
		}
	}
}

game.objects = {}
game.objects.brick = {width: 20, height: 20}
game.objects.brick2 = {width: 10, height: 10}
game.objects.brick3 = {width: 10, height: 10}

//Platform objects
game.platforms = [
	
	{x: 50, y: 450, width: 4000, height: 50, type: "brick", variable: brick, shm: false},
	{x: 120, y: 350, width: 100, height: 20, type: "brick2", variable: brick2, shm: false},
	{x: 350, y: 350, width: 50, height: 50, type: "brick3", variable: brick3, shm: false},

	{x: 550, y: 350, width: 50, height: 10, type: "brick3", variable: brick3, shm: true, shm_info: {shm_in: "y", max_y: 400, min_y: 320, moving_positive: true} },
	{x: 650, y: 350, width: 50, height: 50, type: "brick3", variable: brick3, shm: false},

	{x: 750, y: 450, width: 400, height: 50, type: "brick", variable: brick, shm: false},
	{x: 850, y: 350, width: 50, height: 10, type: "brick2", variable: brick2, shm: false},
	{x: 950, y: 350, width: 50, height: 50, type: "brick", variable: brick, shm: false},
	
];

game.coins = [
	{x: 380, y: 420, width: 20, height: 20, taken: false},
	{x: 430, y: 420, width: 20, height: 20, taken: false},
	{x: 480, y: 420, width: 20, height: 20, taken: false},
	{x: 530, y: 420, width: 20, height: 20, taken: false},
	{x: 580, y: 420, width: 20, height: 20, taken: false},
	{x: 630, y: 420, width: 20, height: 20, taken: false},
	{x: 680, y: 420, width: 20, height: 20, taken: false},
	{x: 730, y: 420, width: 20, height: 20, taken: false},
	{x: 780, y: 420, width: 20, height: 20, taken: false},
	{x: 830, y: 420, width: 20, height: 20, taken: false},
	{x: 880, y: 420, width: 20, height: 20, taken: false},
	{x: 930, y: 420, width: 20, height: 20, taken: false},

];

game.enemies = [
	{x: 100, y: 420, width:30, height:30, shm:{max:400, min:100}, alive: true, going_positive: true},
	{x: 150, y: 420, width:30, height:30, shm:{max:400, min:100}, alive: true, going_positive: true},
	{x: 200, y: 420, width:30, height:30, shm:{max:400, min:100}, alive: true, going_positive: true},
	{x: 250, y: 420, width:30, height:30, shm:{max:400, min:100}, alive: true, going_positive: true},
	{x: 300, y: 420, width:30, height:30, shm:{max:400, min:100}, alive: true, going_positive: true},
]

game.checkpoints = [
	{x: 157, y: 320},
	{x: 1000, y: 300},
	{x: 100000, y: 300},
]

game.checkPointsCleared = 0;



game.draw_enemies = function() {
	for(i=0;i<game.enemies.length;i++) {
		if(game.enemies[i].alive) {
			context.drawImage(enemy, game.enemies[i].x - frame, game.enemies[i].y, game.enemies[i].width, game.enemies[i].height);
		} else {
			if(game.enemies[i].height > 0)
				context.drawImage(enemy, game.enemies[i].x - frame, game.enemies[i].y, game.enemies[i].width, game.enemies[i].height);
		}
	}
}

game.shm_enemies = function() {
	for(i=0;i<game.enemies.length;i++) {
		if(game.enemies[i].alive) {
			if(Math.floor(game.enemies[i].x) == game.enemies[i].shm.min) {
				game.enemies[i].x = game.enemies[i].shm.min + 1;
				game.enemies[i].going_positive = true;
			} else if(Math.floor(game.enemies[i].x) == game.enemies[i].shm.max) {
				game.enemies[i].x = game.enemies[i].shm.max - 1;
				game.enemies[i].going_positive = false;
			} else {
				if(game.enemies[i].going_positive) game.enemies[i].x+=0.3;
				else game.enemies[i].x-=0.3;
			}
		} else {
			if(game.enemies[i].height > 0) {
				game.enemies[i].y += 3
				game.enemies[i].height -= 3
			}
		}
	}
}

game.shm_platforms = function() {
	for(i=0;i<game.platforms.length; i++) {
		if(game.platforms[i].shm == true) {
			if(game.platforms[i].shm_info.shm_in == "x") {
					if(game.platforms[i].x == game.platforms[i].shm_info.max_x) game.platforms[i].shm_info.moving_positive = false;
					if(game.platforms[i].x == game.platforms[i].shm_info.min_x) game.platforms[i].shm_info.moving_positive = true;
					
					if(game.platforms[i].shm_info.moving_positive) game.platforms[i].x += 0.5
					else game.platforms[i].x -= 0.5
				
			} else {
					if(game.platforms[i].y == game.platforms[i].shm_info.max_y) game.platforms[i].shm_info.moving_positive = false;
					if(game.platforms[i].y == game.platforms[i].shm_info.min_y) game.platforms[i].shm_info.moving_positive = true;
					
					if(game.platforms[i].shm_info.moving_positive) game.platforms[i].y += 0.5
					else game.platforms[i].y -= 0.5
				
		
			}
		}
	}
}

game.draw_platforms = function() {
	for(i=0;i<game.platforms.length;i++) {
			var x_start = game.platforms[i].x
			var y_start = game.platforms[i].y
			var x_number = game.platforms[i].width / game.objects[game.platforms[i].type].width;
			var y_number = game.platforms[i].height / game.objects[game.platforms[i].type].height;
			
			
			for(k=0;k<x_number;k++) {
				for(l=0;l<y_number;l++) {
					context.drawImage(game.platforms[i].variable, x_start + game.objects[game.platforms[i].type].width * k - frame, y_start + game.objects[game.platforms[i].type].height * l, game.objects[game.platforms[i].type].width, game.objects[game.platforms[i].type].height)
				}
			}
	}
}

function moveFrame() {
	max_frame = Math.max(frame,game.player.x - game.canvas.width/2)
	frame = max_frame
}

function getKey(event) {
	if(event.charCode == 0) return event.keyCode;
	return event.charCode;
}

function draw() {
	if(showStartUpScreen == true) {
		context.clearRect(0, 0, game.canvas.width, game.canvas.height)
		game.player.draw_player()
		game.shm_platforms()
		writeToHomeScreen()
		game.draw_platforms()
		game.draw_coins()
		game.shm_enemies()
		game.draw_enemies()
		
		} else if(showLives)  {
			context.clearRect(0, 0, game.canvas.width, game.canvas.height)
			context.fillText("Lives left :" + game.player.lives, 100, 100)
			showLives++;
			if(showLives > 500) showLives = 0;
			
		} else {

		moveFrame()
		context.clearRect(0, 0, game.canvas.width, game.canvas.height)
		game.detectCollisions()
		if(game.player.collided_direction == "none") game.player.collided = false;
		game.shm_platforms()
		game.checkForCheckPoints()
		game.player.changeXY()
		game.draw_platforms()
		game.player.draw_player()
		game.detectCollisionWithCoins()
		game.draw_coins()
		game.shm_enemies()
		main_music.play()
		game.draw_enemies()
		game.detectCollisionWithEnemies()	
		if(gameover()) handleGameOver()
			putScore()
	}
}

function gameover() {
//	if(game.player.x < frame - game.player.width) handleGameOver()
//	if(game.player.x < frame) game.player.x = frame;
	if(game.player.y > game.canvas.height) return true;
	if(collidedWithEnemy) {
		collidedWithEnemy = false;
		return true;
	}
}

function handleGameOver() {
	main_music.pause()
	gameover_sound.play()
	showLives = 1;
	if(game.player.lives > 1) {
		game.player.lives -=1;
		frame = 0;
		max_frame = 0;
		game.player.x = game.checkpoints[game.checkPointsCleared-1].x;
		game.player.y = game.checkpoints[game.checkPointsCleared-1].y;
		
		refreshKeyboard()
		
	} else {
//		if(showLives == 0){
		game.player.lives = 0;
		window.location.href +=""
//		}
	}
}

game.draw_coins  = function() {
	for(i=0;i<game.coins.length; i++) {
		if(game.coins[i].taken == false) {
			context.drawImage(coin, game.coins[i].x - frame, game.coins[i].y, game.coins[i].width, game.coins[i].height);
		}
		if(game.coins[i].taken == true && game.coins[i].width > 0) {
			game.coins[i].width = game.coins[i].width - 0.5;
			game.coins[i].y = game.coins[i].y - 3;
			context.drawImage(coin, game.coins[i].x - frame, game.coins[i].y, game.coins[i].width, game.coins[i].height);
		}
	}
}

game.detectCollisionWithCoins = function() {
	for(i=0;i<game.coins.length;i++) {
		if(game.coins[i].taken == false) {
			if(game.detectCollision(game.player, game.coins[i])) {
				coin_taken[i%10].play()
				game.coins[i].taken = true;
				game.player.coins_taken++;
			}
		}
	}
}

game.detectCollisionWithEnemies = function() {
	for(i=0;i<game.enemies.length;i++) {
		if(game.enemies[i].alive) {
			if(game.detectCollision(game.player, game.enemies[i]) && ((game.player.y + game.player.height) - game.enemies[i].y < 5)) {
				game.enemies[i].alive = false;
			} else if(game.detectCollision(game.player, game.enemies[i])) {
				collidedWithEnemy = true;
			}
		}
	}
}

function putScore() {
	document.getElementById("score_coins").innerHTML = "<center> <strong> Coins taken </strong> <br><br>" + game.player.coins_taken + "</center>";
}



canvas.onmousemove = function(event) {
	coordinates.x = (event.clientX - (window.innerWidth - canvas.clientWidth)/2) + 9;
	coordinates.y = event.clientY - 50
} 

function writeToHomeScreen() {
		context.fillStyle = "#000000"
		context.font = "30px Arial"
		context.fillText("Mario Brothers", 100, 100)
		context.strokeStyle = "#FF0000"
		context.strokeText("Mario Brothers", 100, 100)
		context.font= "10px Arial"
		try {
			context.fillText("X: "+ coordinates.x +", Y: "+ coordinates.y, 70, 50)
		} catch(e) {
		
		}
		context.lineWidth = 3
		context.strokeStyle = "#00FF00"
		context.fillRect(400, 100, 150, 50);
		context.strokeRect(400, 100, 150, 50);
		context.lineWidth = 1
		context.font = "30px Arial"
		if(game.detectCollision({x:coordinates.x, y:coordinates.y, width:0, height:0},{x:400, y:100, width: 150, height:50})) {
			context.fillStyle = "#FFFF00"
			if(mouse_pressed) showStartUpScreen = false;
		}
		else context.fillStyle = "#00FF00"
		context.fillText("Start", 435, 135)
		context.fillStyle="#000000"
		
}

game.checkForCheckPoints = function() {
	if(game.player.x > game.checkpoints[game.checkPointsCleared].x) {
		game.checkPointsCleared++;
	}
}

function refreshKeyboard() {
	for(i=0;i<game.keyboard.length;i++) {
		game.keyboard[i] = 0;
	}
}

setInterval(draw, 5)